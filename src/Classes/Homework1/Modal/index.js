import React, {Component} from 'react';
import './Modal.css';
import Button from "../Button";

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clickOut: false,
            clickIn: false
        };
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
    }

    render() {
        const styleClassWindow = this.props.showMod ? 'modal active' : 'modal';
        const styleClassCloseBtn = this.props.closeButton ? 'close active' : 'close';
        return (
            <div className={styleClassWindow} onClick={this.clickOutside}>
                <div className="modal-content" onClick={this.clickInside}>
                    <div className={"header"}>
                        <div className={"header-text"}>{this.props.header}</div>
                        <div className={styleClassCloseBtn}>
                            <Button backgroundColor={'#d44637'} text={'X'} onBClick={this.props.closeModal}/>
                        </div>
                    </div>
                    <p>{this.props.text}</p>
                    <div className={"actions"}>
                        {this.props.actions}
                    </div>
                </div>
            </div>)
    }
}

export default Modal