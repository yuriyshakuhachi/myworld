import React, {Component} from 'react';
import './homework1.css';
import Button from "./Button";
import Modal from "./Modal";

class Homework1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMod: false,
            question: '',
            textHeader: '',
            actions: '',
        };

        this.openFirstModal = this.openFirstModal.bind(this);
        this.openSecondModal = this.openSecondModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.answer1 = this.answer1.bind(this);
        this.answer2 = this.answer2.bind(this);
        this.answer3 = this.answer3.bind(this);
        this.answer4 = this.answer4.bind(this);
    }


    openModal1() {
        this.setState(this.state.modal1);
    }

    openModal2() {
        this.setState(this.state.modal2);
    }

    openFirstModal() {
        this.setState({
            showMod: true,
            textHeader: 'Hi, i am first Modal window',
            question: 'How are you?',
            actions: <>
                <Button backgroundColor={'#0e5b16'} text={'I am fine'} onBClick={this.answer1}/>
                <Button backgroundColor={'#b3382c'} text={'I am bad'} onBClick={this.answer2}/>
            </>
        });
    }

    openSecondModal() {
        this.setState({
            showMod: true,
            textHeader: 'Hi, i am second Modal window',
            question: 'Do you have any question?',
            actions: <>
                <Button backgroundColor={'#0e5b16'} text={'No'} onBClick={this.answer3}/>
                <Button backgroundColor={'#b3382c'} text={'Yes'} onBClick={this.answer4}/>
            </>
        })
    }

    answer1() {
        alert('Good luck!!!')
    }

    answer2() {
        alert('Kill yourself!!!')
    }

    answer3() {
        alert('Bye!')
        this.closeModal()
    }

    answer4() {
        alert("let's go back")
        this.closeModal();
        this.openFirstModal();
    }

    closeModal() {
        this.setState({
            showMod: false,
        })
    }

    render() {
        return <div className="container">
            <Modal showMod={this.state.showMod}
                   closeButton={true}
                   header={this.state.textHeader}
                   text={this.state.question}
                   closeModal={this.closeModal}
                   actions={this.state.actions}
            />
            <Button backgroundColor={'#565d58'} text={'Open first modal'} onBClick={this.openFirstModal}/>
            <Button backgroundColor={'#165d58'} text={'Open second modal'} onBClick={this.openSecondModal}/>
        </div>
    }
}

export default Homework1


