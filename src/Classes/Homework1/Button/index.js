import React, {Component} from 'react';

class Button extends Component {

    render() {
        const style = {
            margin: '5px',
            border: 0,
            backgroundColor: this.props.backgroundColor,
            color: '#ffffff'
        }
        return <button style={style} onClick={this.props.onBClick}>{this.props.text}</button>
    }
}

export default Button
