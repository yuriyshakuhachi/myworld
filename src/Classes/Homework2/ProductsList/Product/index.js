import React, {Component} from 'react';
import "./product.css";
import Button from "../../../Homework1/Button";
import Star from "./Star";

class Product extends Component {
    render() {
        const {id} = this.props.product;
        return <div className="cards">
            <div className="card">{this.props.product.name}</div>
            <div className="card"><img src={this.props.product.url} alt=""/></div>
            <div className="card">{this.props.product.price}$</div>
            <div className="button">
                <Button backgroundColor={'#206a72'} text={this.props.buttonText} onBClick={
                    () => {
                        this.props.openModal(this.props.product, 'Add to cart?', 1);
                    }}/>
            </div>
            <div className="button">
                <Star color={'#ff8400'} backgroundColor={'#888888'} id={id} onBClick={
                    () => {
                        this.props.addToFaw(id);
                    }}/>
            </div>
        </div>
    }
}

export default Product
