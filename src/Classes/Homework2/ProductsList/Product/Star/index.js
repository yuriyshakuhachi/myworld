import React, {Component} from 'react';

class Star extends Component {
    constructor(props) {
        super(props);
        let text = '';
        if (localStorage.getItem(`${this.props.id}.faw`) === 'true') {
            text = '\u2605'
        } else {
            text = '\u2606'
        }
        this.state = {
            text: text
        }
        this.favChange = this.favChange.bind(this)
    }

    render() {
        const style = {
            margin: '5px',
            border: 0,
            backgroundColor: this.props.backgroundColor,
            color: this.props.color,
        }
        return <button style={style}
                       onClick={this.favChange}>{this.state.text}
        </button>
    }

    favChange() {
        this.props.onBClick();
        let text = '';
        if (localStorage.getItem(`${this.props.id}.faw`) === 'true') {
            text = '\u2605'
        } else {
            text = '\u2606'
        }
        this.setState({
            text: text
        })
    }
}

export default Star