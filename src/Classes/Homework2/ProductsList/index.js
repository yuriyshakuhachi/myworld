import React, {Component} from 'react';
import Product from "./Product";

class ProductList extends Component {

    render() {

        if (this.props.products !== '!!!') {
            return (
                <div>
                    {
                        this.props.products.map(product => <Product product={product}
                                                                    buttonText={this.props.buttonText}
                                                                    addToFaw={this.props.addToFaw}
                                                                    openModal={this.props.openModal}
                                                                    key={product.id}/>)
                    }
                </div>
            )
        } else {
            return <></>
        }
    }
}

export default ProductList;