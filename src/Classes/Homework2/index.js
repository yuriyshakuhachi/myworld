import React, {Component} from 'react';
import ProductList from "./ProductsList";
import Modal from "../Homework1/Modal";
import Button from "../Homework1/Button";


class Homework2 extends Component {
    constructor(props) {
        super(props);
        console.log('props constructor H2--->',this.props.products)
        this.state = {
            buttonText: 'Add to cart?',
            products: this.props.products,
            showMod: false,
        };

        this.closeModal = this.closeModal.bind(this);
        this.openModal = this.openModal.bind(this);
        this.addToCart = this.addToCart.bind(this);
        this.addToFaw = this.addToFaw.bind(this);
    }

    render() {

        return <>

            <div className="container">
                <Modal showMod={this.state.showMod}
                       closeButton={true}
                       header={this.state.header}
                       text={this.state.text}
                       closeModal={this.closeModal}
                       actions={this.state.actions}
                />
            </div>

            <div className="main-container">
                <div className="container">
                    <ProductList products={this.state.products}
                                 buttonText={this.state.buttonText}
                                 openModal={this.openModal}
                                 addToFaw={this.addToFaw}
                                 key={this.state.products.id}/>
                </div>
            </div>
        </>
    }

    openModal({id, color, name, price, url}, header, act) {
        /**
         * Если это модальное окно для добавления продукта в корзину
         * @type {JSX.Element}
         */
        if (act === 1) {
            let actions = <>
                <Button backgroundColor={'#b3382c'} text={'No'} onBClick={() => {
                    this.closeModal()
                }}/>
                <Button backgroundColor={'#0e5b16'} text={'Yes'} onBClick={() => {
                    this.addToCart();
                }}/>
            </>

            this.setState({
                showMod: true,
                header: header,
                id: id,
                color: color,
                url: url,
                text: `"${name}" price: ${price}$`,
                actions: actions
            })
        }
        /**
         * если это Модальное окно корзины
         */
        else {
            let actions = <>
                <Button backgroundColor={'#b3382c'} text={'No'} onBClick={() => {
                    this.closeModal()
                }}/>
                <Button backgroundColor={'#0e5b16'} text={'Yes'} onBClick={() => {
                    this.ClearCart();
                }}/>
            </>

            this.setState({
                showMod: true,
                header: header,
                id: id,
                color: color,
                url: url,
                text: `"${name}" price: ${price}$`,
                actions: actions
            })

        }
    }

    addToCart() {
        this.closeModal();
        if ((localStorage.getItem(`${this.state.id}.cart`) === null)||(localStorage.getItem(`${this.state.id}.cart`) === '0')) {
            console.log('ini');
            localStorage.setItem(`${this.state.id}.cart`, '1');
        } else {
            localStorage.setItem(`${this.state.id}.cart`,
                String(parseInt(localStorage.getItem(`${this.state.id}.cart`)) + 1));
        }
        console.log(localStorage)
    }

    addToFaw(id) {
        if ((localStorage.getItem(`${id}.faw`) === null) || (localStorage.getItem(`${id}.faw`) === 'false')) {
            console.log('ini faw true');
            localStorage.setItem(`${id}.faw`, 'true');
        } else {
            localStorage.setItem(`${id}.faw`, 'false');
            console.log('ini faw false');
        }
    }


    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
    }

    closeModal() {
        this.setState({
            showMod: false
        })
    }
}

export default Homework2