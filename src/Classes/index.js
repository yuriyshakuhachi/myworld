import React from 'react';
import Main from "./Main";
import Header from "./Header";
import * as axios from "axios";

class Classes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: '!!!',
        }
    }

    render() {
        return (
            <div className="App">
                <Header/>
                <Main products={this.state.products}/>
            </div>
        )
    }

    componentDidMount() {
        this.setProducts()
    }

    setProducts() {
        axios("http://localhost:3000/products.json")

            .then((response) => {
                this.setState((state) => {
                    const products = response.data;
                    return {products}
                });
            })
            .catch((error) => {
                console.log(error);
            });

    }


}

export default Classes