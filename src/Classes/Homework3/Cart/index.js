import Homework2 from "../../Homework2";
import Button from "../../Homework1/Button";

/**
 * Cart
 */

class Cart extends Homework2 {
    constructor(props) {
        super(props);
        if (this.props.products !== '!!!') {

            let cartProducts = this.props.products.filter((item) => {
                if ((localStorage.getItem(`${item.id}.cart`) !== null) && (localStorage.getItem(`${item.id}.cart`) !== '0')) {
                    return item
                }
            });

            this.state = {
                buttonText: 'Remove from cart?',
                products: cartProducts,
                showMod: false,
            };

            this.closeModal = this.closeModal.bind(this);
            this.openModal = this.openModal.bind(this);
            this.removeFromCart = this.removeFromCart.bind(this);
        }
    }

    removeFromCart() {
        console.log('remove from cart');
        this.closeModal();
        localStorage.setItem(`${this.state.id}.cart`,
            String(parseInt(localStorage.getItem(`${this.state.id}.cart`)) - 1));
        Cart.cartProducts = this.props.products.filter((item) => {
            if ((localStorage.getItem(`${item.id}.cart`) !== null) && (localStorage.getItem(`${item.id}.cart`) !== '0')) {
                return item
            }
        });
        this.setState({products: Cart.cartProducts})
        console.log(localStorage)
    }

    openModal({id, color, name, price, url}, header, act) {
        header = 'remove from cart?'
        let actions = <>
            <Button backgroundColor={'#b3382c'} text={'No'} onBClick={() => {
                this.closeModal()
            }}/>
            <Button backgroundColor={'#0e5b16'} text={'Yes'} onBClick={() => {
                this.removeFromCart();
            }}/>
        </>

        this.setState({
            showMod: true,
            header: header,
            id: id,
            color: color,
            url: url,
            text: `"${name}" price: ${price}$`,
            actions: actions
        })

    }
}

export default Cart
