import Homework2 from "../../Homework2";

class Favorite extends Homework2 {
    constructor(props) {
        super(props);
        if (this.props.products !== '!!!') {
            let favProducts = this.props.products.filter((item) => {
                if (localStorage.getItem(`${item.id}.faw`) === "true") {
                    return item
                }
            });

            this.state = {
                buttonText: 'Add to cart?',
                products: favProducts,
                showMod: false,
            };

            this.closeModal = this.closeModal.bind(this);
            this.openModal = this.openModal.bind(this);
        }
    }

}

export default Favorite
