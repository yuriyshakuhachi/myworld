import React, {Component} from 'react';
import Homework2 from "../Homework2";
import Cart from "../Homework3/Cart";
import Favorite from "../Homework3/Favorite";
import {Switch, Route} from 'react-router-dom';

class Main extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        console.log('Main--->', this.props.products);
        return (
            <Switch>
                <Route exact path='/' component={() => <Homework2 products={this.props.products}/>}>
                </Route>
                <Route path='/cart' component={() => <Cart products={this.props.products}/>}>
                </Route>
                <Route path='/favorite' component={() => <Favorite products={this.props.products}/>}>
                </Route>
            </Switch>)

    }

}

export default Main
